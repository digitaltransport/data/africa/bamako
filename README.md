# GTFS Transport Public Bamako

**Scope Of Work**

Bamako is a very large city by its size and population. It is served mainly by several modes of public transport such as mini-cars with a capacity of 12 - 25 seats called "SOTRAMA". The SOTRAMA network serves the city with buses of 12-25 people capacity on certain assigned routes and certain unofficial, tolerated routes. The price for a trip ranges between 175 FCFA and 350 FCFA. The current network is mapped in some capacity but not sufficiently. The planned OpenStreetMap coverage of the transit network will cater two main objectives:

1.to identify possibilities for improvement of the transit system in the Malian capital, and

2.to make data about the SOMATRA network accessible to users in the city and the municipal administration and the union itself.

Additionally, this activity will also cater to the following collateral objectives:

-to build capacity on (open data) mapping in the city and iv) to create awareness and encourage coordination between transit stakeholders in Bamako. The consultancy is therefore expected to collect the relevant data for the whole city / SOTRAMA
network and put them at the disposal of end users and the client. The data to be collected should include the following aspects of the network:

-stops (large intersections, landmarks) ;
-trip data (start/end-trip, trip time, associated costs for different sections of the travel);
-data sets in Shapefile and GTFS formats.

**Stakeholders involved in the project**

**[The World Bank](https://www.gfdrr.org/en)** through GFDRR (the Global Facility for Disaster Reduction and Recovery) is the institution that started and funded the public transport mapping project in Mali.

**[Data Transport](https://data-transport.org/)** through Startup Billet Express was in charge of the implementation of the project.

**[OpenStreetMap Mali](https://www.facebook.com/Openstreetmap-Mali-909615379063102)** was in charge of the data collection and mapping of Sotrama bus lines.

**GTFS Dataset Production**

The GTFS was created by the non-profit organization **Data Transport** through the open source web application [Watrifeed](https://watrifeed.ml/). 
The GTFS dataset is composed of :
 **105 bus lines;
 3457 bus stops;
 210 trips;
 8826 stop times;
 210 frequencies;
 50712 shapes.**

The GTFS data for Bamako's public transport network can be visualized online through the application [TransitViz.org](https://transitviz.org/).


